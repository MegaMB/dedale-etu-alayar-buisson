package utilitaires;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.env.Location;
import utilitaires.GoldLoc.Statut;

public class GoldList {
	private List<GoldLoc> element;
	
	public GoldList() {
		element = new ArrayList<GoldLoc>();
	}
	
	public void add(Location l, int g, int o, int lo, int s) {
		GoldLoc addGold = new GoldLoc(l, g, o, lo, s);
		boolean check = false;
		for (GoldLoc el : this.element) {
			if (el.maj(addGold)) {
				check = true;
				break;
			}
		}
		if (check == false) {
			this.element.add(addGold);
		}
	}
	
	/**
	 * Verifie si un trésor se trouve sur son emplacement ou pas.
	 */
	public boolean find(Location myLocation) {
		if (element.isEmpty()){
			return false;
		}
		for (GoldLoc el : this.element) {
			//System.out.println("Comparing "+ el.getLocation().getLocationId()+ " with "+myLocation.getLocationId()+ "\n");
			if (el.getLocation().getLocationId() == myLocation.getLocationId()) {
				//System.out.println("Found match here");
				return true;
			}
		}
		return false;
	}
	
	/**
	 * A faire lorsque l'on essaie de ramasser un trésor et que celui-ci n'est plus là.
	 */
	public void notFounded(Location myLocation) {
		for (GoldLoc el : this.element) {
			//System.out.println("Comparing "+ el.getLocation().getLocationId()+ " with "+myLocation.getLocationId()+ "\n");
			if (el.getLocation().getLocationId() == myLocation.getLocationId() && (el.getEtat() == Statut.PRESENT)) {
				el.setEtat(Statut.DEPLACE);
				//System.out.println("Found match here");
				return ;
			}
		}
	}
	
	/**
	 * A faire lors du ramassage d'un trésor, pour noter qu'il est ramassé.
	 */
	public void ramasse(Location myLocation) {
		for (GoldLoc el : this.element) {
			//System.out.println("Comparing "+ el.getLocation().getLocationId()+ " with "+myLocation.getLocationId()+ "\n");
			if (el.getLocation().getLocationId() == myLocation.getLocationId()) {
				el.setEtat(Statut.RAMASSE);
				//System.out.println("Found match here");
				return ;
			}
		}
	}
	
	@Override
	public String toString() {
		String res = new String();
		for (GoldLoc el : this.element) {
			res = res+el.getLocation().getLocationId()+" ";
		}
		return res;
	}
}
