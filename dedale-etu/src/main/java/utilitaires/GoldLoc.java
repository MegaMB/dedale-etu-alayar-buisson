package utilitaires;

import eu.su.mas.dedale.env.Location;

public class GoldLoc {
	private Location loc;
	private int gold;
	private int open;
	private int lockpick;
	private int strength;
	Statut etat;
	
	public enum Statut {
		PRESENT,
		DEPLACE,
		RAMASSE,
	}
	
	public GoldLoc(Location l, int g, int o, int lo, int s) {
		this.loc = l;
		this.gold = g;
		this.open = o;
		this.lockpick = lo;
		this.strength = s;
		this.etat = Statut.PRESENT;
	}
	
	//Si on rencontre un gold avec les mêmes attributs, mais à un autre endroit ou bien refermé,
	//c'est parce que c'est le même gold déplacé par le wumpus.
	public boolean maj(GoldLoc otherGold) {
		if (this.gold == otherGold.gold && this.lockpick == otherGold.lockpick && this.strength == otherGold.strength) {
			this.loc = otherGold.loc;
			this.open = otherGold.open;
			this.etat = Statut.PRESENT;
			return true;
		}
		return false;
	}
	
	public Location getLocation() {
		return this.loc;
	}
	
	public Statut getEtat() {
		return this.etat;
	}
	
	public void setEtat(Statut s) {
		this.etat = s;
	}
}
