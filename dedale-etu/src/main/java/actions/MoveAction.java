package actions;

import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.gs.gsLocation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import jade.core.behaviours.OneShotBehaviour;

public class MoveAction extends OneShotBehaviour {
	private FSMExploBehaviour caller;
	private int success;
	
	public MoveAction (FSMExploBehaviour b) {
		this.caller = b;
		this.success = 1;
	}

	@Override
	public void action() {
		this.success = 1;
		String nextNodeId = caller.getNextNodeId();
		//System.out.print("Début de MoveAction\n");
		if (nextNodeId != null) {
			((AbstractDedaleAgent)this.myAgent).moveTo(new gsLocation(nextNodeId));
			//System.out.print("Mouvement effectué\n");
		}
		
		Location myPosition = ((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (nextNodeId != myPosition.getLocationId()) {
			success = 0;
		}
	}
	
	public int onEnd(){
		return success;
	}

}
