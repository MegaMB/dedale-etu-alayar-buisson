package actions;

import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class EmitCollusAction extends OneShotBehaviour {
	private List<String> receivers;
	private FSMExploBehaviour caller;
	
	public EmitCollusAction(FSMExploBehaviour c, List<String> a) {
		this.receivers = a;
		this.caller = c;
	}

	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setProtocol("COLLUSION");
		msg.setSender(this.myAgent.getAID());
		for (String agentName : receivers) {
			msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
		}		
		msg.setContent(this.caller.getNextNodeId());
		//System.out.print("Collusion, j'essaie d'aller en "+this.caller.getNextNodeId()+"\n");
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		this.caller.setPrioCollus(true);

	}

}
