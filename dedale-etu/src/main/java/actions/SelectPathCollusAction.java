package actions;

import java.util.Iterator;
import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import jade.core.behaviours.OneShotBehaviour;

public class SelectPathCollusAction extends OneShotBehaviour {
	private FSMExploBehaviour caller;
	
	public SelectPathCollusAction (FSMExploBehaviour b) {
		this.caller=b;
	}

	@Override
	public void action() {
		String IDToAvoid = caller.getMap().getShortestPathToClosestOpenNode(this.caller.getPosition().getLocationId()).get(0);
		List<Couple<Location,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
		
		String nextNodeId=null;
		Iterator<Couple<Location, List<Couple<Observation, Integer>>>> iter = lobs.iterator();
		//System.out.print("Il ne faut pas que j'aille en "+IDToAvoid);
		while(iter.hasNext()){
			Location potentialNextNod = iter.next().getLeft();
			if (potentialNextNod.getLocationId() != IDToAvoid) {
				nextNodeId = potentialNextNod.getLocationId();
				this.caller.setNextNodeId(nextNodeId);
				//System.out.print("Du coup je vais en "+nextNodeId);
				this.caller.setPrioCollus(false);
				break;
			}
		}
	}

}
