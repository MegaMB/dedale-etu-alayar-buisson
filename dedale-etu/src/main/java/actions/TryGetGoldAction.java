package actions;

import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.OneShotBehaviour;
import utilitaires.GoldList;
import utilitaires.GoldLoc;

public class TryGetGoldAction extends OneShotBehaviour {
	private FSMExploBehaviour caller;
	private Location myLocation;
	
	public TryGetGoldAction (FSMExploBehaviour b) {
		this.caller=b;
	}

	@Override
	public void action() {
		this.myLocation = this.caller.getPosition();
		GoldList myGoldList = this.caller.getGoldList();
		if (myGoldList == null) {
			myGoldList = new GoldList();
			this.caller.setGoldList(myGoldList);
		}
		//System.out.println("Check Try get Gold, I'm on"+ myLocation.getLocationId()+" And I'm looking amongst :"+myGoldList.toString()+ "\n");
		boolean GoldOnMyPosition = myGoldList.find(myLocation);
		if (GoldOnMyPosition == true) {
			System.out.println("Gold on my position\n");
			if (((AbstractDedaleAgent)this.myAgent).openLock(Observation.GOLD)) {
				((AbstractDedaleAgent)this.myAgent).pick();
				System.out.println("Gold picked up\n");
				myGoldList.ramasse(myLocation);
			} else {
				System.out.println("Could not pick up Gold\n");
				myGoldList.notFounded(myLocation);
			}
		}
	}

}
