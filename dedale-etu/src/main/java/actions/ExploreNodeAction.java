package actions;

import java.util.Iterator;
import java.util.List;

import javax.swing.event.DocumentEvent.ElementChange;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.OneShotBehaviour;
import utilitaires.GoldList;

public class ExploreNodeAction extends OneShotBehaviour {
	private FSMExploBehaviour caller;
	private MapRepresentation myMap;
	private int finished;
	
	public ExploreNodeAction (FSMExploBehaviour b) {
		this.caller = b;
		this.finished = 1;
	}

	@Override
	public void action() {
		//System.out.print("Début d'ExploreNodeAction\n");
		Location myPosition = ((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		this.myMap=caller.getMap();
		
		if (myPosition == null) {
			System.out.print("GROS SOUCIS\n");
		} else {
			caller.setPosition(myPosition);
			
			//List of observable from the agent's current position
			List<Couple<Location,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//1) remove the current node from openlist and add it to closedNodes.
			this.myMap.addNode(myPosition.getLocationId(), MapAttribute.closed);

			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
			String nextNodeId=null;
			Iterator<Couple<Location, List<Couple<Observation, Integer>>>> iter = lobs.iterator();
			while(iter.hasNext()){
				//Exploration des points autours
				Couple<Location, List<Couple<Observation, Integer>>> couple = iter.next();
				Location accessibleNode = couple.getLeft();
				boolean isNewNode = this.myMap.addNewNode(accessibleNode.getLocationId());
				//the node may exist, but not necessarily the edge
				if (myPosition.getLocationId()!=accessibleNode.getLocationId()) {
					this.myMap.addEdge(myPosition.getLocationId(), accessibleNode.getLocationId());
					if (nextNodeId == null && isNewNode) nextNodeId=accessibleNode.getLocationId();
				}
				//TODO: Exploration des coffres autours
				//System.out.print("element = "+couple.getRight().size());
				if (couple.getRight().size() != 0) {
					this.addGoldToList(couple.getRight(), couple.getLeft());
					//Iterator<Couple<Observation, Integer>> element = couple.getRight().iterator();
					//while(element.hasNext()) {
					//	Couple<Observation, Integer> coupleElement = element.next();
					//	Observation elementObs = coupleElement.getLeft();
					//	Integer elementVal = coupleElement.getRight();
					//	GoldList gl=caller.getGoldList();
					//	if (gl == null) {
					//		gl = new GoldList();
					}
						//System.out.print("Valeur de l'observations: "+elementObs+" et valeur de l'int: "+elementVal+"\n");
				}
				//TODO: Exploration des agents autours
			caller.setNextNodeId(nextNodeId);
			//System.out.println(caller.getNextNodeId()+" Est la prochaine case à explorer\n");
			//Mise à jour de la carte FSM
			caller.setMap(myMap);
			
			if (!this.myMap.hasOpenNode()){
				//Explo finished
				int finished = 0;
				System.out.println(this.myAgent.getLocalName()+" - Exploration successfully done, behaviour removed.");
			} else {
				int finished = 1;
				//System.out.print("This can continue\n");
				}
		}
	}
	
	public void addGoldToList (List<Couple<Observation, Integer>> info, Location loc) {
		Iterator<Couple<Observation, Integer>> element = info.iterator();
		if (!element.hasNext()) {
			return ;
		}
		Couple<Observation, Integer> couple = element.next();
		if (couple.getLeft().toString()!="Gold") {
			return ;
		}
		int gold = couple.getRight();
		element.next();
		int lockop = couple.getRight();
		element.next();
		int lockpi = couple.getRight();
		element.next();
		int strength = couple.getRight();
		GoldList gl = this.caller.getGoldList();
		if (gl == null) {
			gl = new GoldList();
		}
		gl.add(loc, gold, lockop, lockpi, strength);
		this.caller.setGoldList(gl);
		//System.out.print("Gold sized : "+gold+" added\n");
		return;
	}
	
	public int onEnd() {
		if (this.caller.getPrioCollus() == true) {
			return 1;
		}
		return 0;
		
	}
}
