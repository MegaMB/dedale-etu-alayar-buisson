package actions;

import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.OneShotBehaviour;

public class CreateMapAction extends OneShotBehaviour {
	private FSMExploBehaviour caller;
	
	public CreateMapAction (FSMExploBehaviour b) {
		this.caller=b;
	}

	@Override
	public void action() {
		if(caller.getMap()==null) {
			System.out.print("CreatingMap\n");
			caller.setMap( new MapRepresentation());
			System.out.print(caller.getMap().toString());
			System.out.print(" End of Creating Map\n");
		} else {
			System.out.print("Map déjà crée\n");
		}
	}

}
