package actions;

import java.io.IOException;
import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class EmitPingAction extends OneShotBehaviour {
	private List<String> receivers;
	private FSMExploBehaviour caller;
	
	public EmitPingAction(FSMExploBehaviour c, List<String> a) {
		this.receivers = a;
		this.caller = c;
	}

	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setProtocol("PING");
		msg.setSender(this.myAgent.getAID());
		for (String agentName : receivers) {
			msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
		}
		
		MapRepresentation map = caller.getMap();			
		msg.setContent(Integer.toString(this.caller.getMap().getSizeMap()));
		//System.out.print("Ping, map size: "+map.getSizeMap()+"\n");
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);

	}

}
