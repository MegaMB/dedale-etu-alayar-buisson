package actions;

import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import jade.core.behaviours.OneShotBehaviour;

public class SelectPathAction extends OneShotBehaviour {
	private FSMExploBehaviour caller;
	
	public SelectPathAction (FSMExploBehaviour b) {
		this.caller=b;
	}

	@Override
	public void action() {
		if (caller.getNextNodeId() == null) {
			Location position = caller.getPosition();
			caller.setNextNodeId(caller.getMap().getShortestPathToClosestOpenNode(position.getLocationId()).get(0));
		}
	}

}
