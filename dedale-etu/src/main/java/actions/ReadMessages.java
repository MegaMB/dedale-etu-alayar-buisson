package actions;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ReadMessages extends OneShotBehaviour {
	private FSMExploBehaviour caller;
	
	public ReadMessages (FSMExploBehaviour b) {
		this.caller = b;
	}
	
	@Override
	public void action() {
		System.out.print("iciiiiiiiiiiiiiiiii");
		//System.out.print("Let's try to read new maps");
		MessageTemplate mapMsgTemplate=MessageTemplate.and(
				MessageTemplate.MatchProtocol("SHARE-TOPO"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage mapMsgReceived=this.myAgent.receive(mapMsgTemplate);
		while (mapMsgReceived!=null) {
			SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
			try {
				sgreceived = (SerializableSimpleGraph<String, MapAttribute>)mapMsgReceived.getContentObject();
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			MapRepresentation map = caller.getMap();
			map.mergeMap(sgreceived);
			caller.setMap(map);
			mapMsgReceived=this.myAgent.receive(mapMsgTemplate);
		}
		//System.out.print("Let's try to read new pings");
		MessageTemplate pingMsgTemplate=MessageTemplate.and(
				MessageTemplate.MatchProtocol("PING"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage pingMsgReceived=this.myAgent.receive(pingMsgTemplate);
		while (pingMsgReceived!=null) {
			String pingContent = pingMsgReceived.getContent();
			System.out.print("Message Reçu par "+ this.myAgent.getLocalName()+" : ");
			System.out.print(pingContent);
			System.out.print("\n");
			pingMsgReceived=this.myAgent.receive(pingMsgTemplate);
		}
	}

}
