package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import actions.CreateMapAction;
import actions.EmitCollusAction;
import actions.EmitPingAction;
import actions.ExploreNodeAction;
import actions.MoveAction;
import actions.ReadMessages;
import actions.SelectPathAction;
import actions.SelectPathCollusAction;
import actions.TryGetGoldAction;
import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.gs.gsLocation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.communication.FSMCommunication;
import eu.su.mas.dedaleEtu.mas.behaviours.ShareMapBehaviour;


import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.FSMBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import utilitaires.GoldList;



public class FSMExploBehaviour extends FSMBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;
	private boolean finished = false;
	private MapRepresentation myMap;
	private List<String> list_agentNames;
	private Location Position;
	private String nextNodeId;
	private GoldList goldList;
	private boolean priorite_collusion;
	
	//Constructor
	public FSMExploBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap,List<String> agentNames) {
		super(myagent);
		this.myMap = myMap;
		this.list_agentNames = agentNames;
		this.Position = null;
		this.nextNodeId = null;
		this.goldList = null;
		this.priorite_collusion=false;
		
		super.registerFirstState(new CreateMapAction(this), "init_map");
		super.registerState(new FSMCommunication(myagent, this), "read_messages");
		super.registerState(new ExploreNodeAction(this), "register_location");
		super.registerState(new TryGetGoldAction(this), "pick_up");
		super.registerState(new SelectPathAction(this), "select_next_location");
		super.registerState(new EmitPingAction(this, list_agentNames), "emits_ping");
		super.registerState(new MoveAction(this), "move_to_next_location");
		super.registerState(new EmitCollusAction(this, list_agentNames), "emits_collusion");
		super.registerState(new SelectPathCollusAction(this), "select_collus_location");
		
		super.registerDefaultTransition("init_map", "read_messages", null);
		super.registerDefaultTransition("read_messages", "register_location", new String[]{"read_messages"});
		
		super.registerDefaultTransition("register_location", "select_next_location", null);
		super.registerDefaultTransition("select_next_location", "emits_ping", null);
		super.registerDefaultTransition("emits_ping", "pick_up", null);
		super.registerDefaultTransition("pick_up", "move_to_next_location", null);
		super.registerDefaultTransition("move_to_next_location", "read_messages", null);
		
		//Cas où il y a une collusion
		super.registerTransition("move_to_next_location", "emits_collusion", 0);
		super.registerDefaultTransition("emits_collusion", "read_messages");
		
		super.registerTransition("register_location", "select_collus_location", 1);
		super.registerDefaultTransition("select_collus_location", "emits_ping");
	}
	
	//Getters and setters
	public void setMap(MapRepresentation m) {
		this.myMap = m;
	}
	public MapRepresentation getMap() {
		return this.myMap;
	}
	
	public void setPosition(Location l) {
		this.Position = l;
	}
	
	public Location getPosition() {
		return this.Position;
	}
	
	public void setNextNodeId(String s) {
		this.nextNodeId = s;
	}
	
	public String getNextNodeId() {
		return this.nextNodeId;
	}
	
	public List<String> getListAgent() {
		return this.list_agentNames;
	}
	
	public GoldList getGoldList() {
		return this.goldList;
	}
	
	public void setGoldList(GoldList l) {
		this.goldList = l;
	}
	
	public boolean getPrioCollus() {
		return this.priorite_collusion;
	}
	
	public void setPrioCollus(boolean b) {
		this.priorite_collusion = b;
	}
}
