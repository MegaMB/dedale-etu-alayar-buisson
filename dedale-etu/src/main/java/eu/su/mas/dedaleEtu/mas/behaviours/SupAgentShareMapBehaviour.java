package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;

import java.util.ArrayList;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
//import jade.util.leap.ArrayList;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class SupAgentShareMapBehaviour extends SimpleBehaviour{
	private MapRepresentation myMap;
	private boolean finished = false;
	private boolean sent = false;
	private boolean received = false;
	private int time;
	private String Sender;
	private List<String> list_agentNames;

	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to share his Map
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public SupAgentShareMapBehaviour (Agent a,  MapRepresentation myMap, String Sender, List<String> list_agentNames) {
		//super(a, period);
		super(a);
		this.Sender = Sender;
		this.myMap = myMap;
		this.list_agentNames=list_agentNames;
		this.time = 0;

	}
	
	
	@Override
	//public void onTick() {
	public void action() {
		

		
		if (!this.sent) {
			// partage Map
			List<String> list_agentName = new ArrayList<String>(); 
			list_agentName.add(this.Sender);								
			this.myAgent.addBehaviour(new ShareMapBehaviour(this.myAgent,500,this.myMap,list_agentName));		
			System.out.println("Envoie : Agent "+this.myAgent.getLocalName()+ " ===>" + this.Sender+ " :  Map1");
			this.sent = true;
		}
				
		// s'il reçoit la map de l'autre agent, il la lit et adapte sa Map en fonction
		// à mettre un petit block
		// à ajouter la comparaison de taille map et la traiter ...
		
		// *** à faire : ajouter à la template un filtre des senders 
		MessageTemplate msgTemplate=MessageTemplate.and(
				MessageTemplate.MatchProtocol("SHARE-TOPO"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived=this.myAgent.receive(msgTemplate);
				
		
		// l'agent attend la map 30ms maximum, en cas d'absence, il reprend son exploration 
		try {
			this.myAgent.doWait(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.time += 1;
		System.out.println("time : " + this.time);
		
		if(this.time > 30) {
			System.out.println("Agent "+this.myAgent.getLocalName()+ ": map2 non reçue **** time : "+time);
			this.finished = true; 
			this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); 
		}

		else if (msgReceived!=null) {


			if(! this.received) {  // on peut l'enlever ***
				System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + msgReceived.getSender().getLocalName()+ " :  Map2 ***** time : "+this.time);
				
				SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
				try {
					sgreceived = (SerializableSimpleGraph<String, MapAttribute>)msgReceived.getContentObject();
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.myMap.mergeMap(sgreceived);		
				this.finished = true; 
				this.received = true;
				
				// reprend l'exploration
				this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); 
			}
		}
		
		
	} 
	@Override
	public boolean done() {
		return this.finished;
	}
}
		
		
		
		
		
		
		
		
		
