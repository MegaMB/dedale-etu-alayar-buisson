package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.WakerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.util.ArrayList;
//import jade.util.leap.ArrayList;
import eu.su.mas.dedale.env.gs.gsLocation;



/**
 * 
 * @author hc
 *
 */
//public class PingEmissionBehaviour extends WakerBehaviour{
public class MeetSiloBehaviour extends OneShotBehaviour{
	private MapRepresentation myMap;
	
	private List<String> list_agentNames;
	
	private List<List<String>> list_agentsWithCompleteMap;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134632075521548L;
	/**
	 * l'agent essai de rencontrer le Silo, et réagir en fonction de ce que le Silo lui renvoie
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public MeetSiloBehaviour (Agent agent, List<String> list_agentNames, MapRepresentation myMap,List<List<String>> list_agentsWithCompleteMap ) {
		super(agent);
		this.list_agentNames=list_agentNames;
		this.list_agentsWithCompleteMap =list_agentsWithCompleteMap; // L=[[les agents],[leurs capacités]] (agent L[0][2] à la capacité L[1][2]) ** possible de le modifier
		this.myMap = myMap;
	}

	@Override
	public void action(){
		
		// calcul de la position du Silo, provisoirement 24  *** à modifier
		Location SiloPosition = new gsLocation("24");
		
		Location myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		List<String> chemin=this.myMap.getShortestPath(myPosition.getLocationId(),SiloPosition.getLocationId());
		
		// il s'arrete sur la case voisine du Silo
		int i = 0;
		while(i<chemin.size()-1) {
			// faut traiter les inters blocage ici
			((AbstractDedaleAgent)this.myAgent).moveTo(new gsLocation(chemin.get(i)));
			i=i+1;
		}
		
		// essaie de renter en contact avec le Silo
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("ShareCapacities");
		msg.setContent("Hello Tanker");
		msg.addReceiver(new AID("Tanker",AID.ISLOCALNAME));
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);

		// les templates pour les messages possibles
		final MessageTemplate msgTemplate1 = MessageTemplate.and(MessageTemplate.MatchSender(new AID("Tanker",AID.ISLOCALNAME)),MessageTemplate.MatchProtocol("Share list_agentsWithCompleteMap"));
		ACLMessage reponse1 = this.myAgent.receive(msgTemplate1);
		
		final MessageTemplate msgTemplate2 = MessageTemplate.and(MessageTemplate.MatchSender(new AID("Tanker",AID.ISLOCALNAME)),MessageTemplate.MatchProtocol("Share strategie"));
		ACLMessage reponse2 = this.myAgent.receive(msgTemplate2);
		
		// attendre une reponse du Tanker (maximu 3s)
		boolean stop = false;
		int wait = 0;		
		while(wait<3000 && !stop) {
			try {
				this.myAgent.doWait(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (reponse1 !=null || reponse2 != null) {
				stop = true;				
			}
			else {
				reponse1 = this.myAgent.receive(msgTemplate1);
				reponse2 = this.myAgent.receive(msgTemplate2);
			}
			wait = wait + 1;
		}
		// le Silo n'est pas à sa place, il n'a pas fini l'exploration
		if (reponse1 == null && reponse2 == null) {
			// remplacer le Silo jusqu'à ce qu'il arrive
			//this.myAgent.addBehaviour(new TankerShareMapBehaviour(this.myAgent, this.myMap, this.list_agentNames, this.list_agentsWithCompleteMap, SiloPosition));
		}
		else {
			// traitement du message reçu
			// messages possibles : liste, strategie 
			
			// cas 1 : liste des agents ayant la carte
			// Le tanker (ou son remplaçant) partage sa list_agentsWithCompleteMap, le partage se fait avec le protocole "Share list_agentsWithCompleteMap", 
			// l'agent la lui complète si possible puis commence à partager sa map et sa liste avec les autres agents
			if(reponse1 != null) { 
				
				// récuperer la liste
				List<List<String>> listeReceived=null;
				try {
					listeReceived = (List<List<String>>)reponse1.getContentObject();
					//jade.util.leap.List listeReceived = (jade.util.leap.List)reponse1.getContentObject();
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				// petit test pour la verification
				if(listeReceived.get(0).size()>0) { System.out.println("liste de Silo est vide");}
				
				// les elements qui manquent au silo
				List<List<String>> listeToSend=  new ArrayList<>();
				List<String> L1 = new ArrayList<>();
				List<String> L2 = new ArrayList<>();
				listeToSend.add(L1);
				listeToSend.add(L2);
						
				for(int j=0; j<this.list_agentsWithCompleteMap.get(0).size();j=j+1) {
					String tmp1 =this.list_agentsWithCompleteMap.get(0).get(j);
					String tmp2 =this.list_agentsWithCompleteMap.get(1).get(j);
					// si l'agent n'existe pas dans sa liste, il l'ajoute, ainsi que ses capacités 
					if(!listeReceived.get(0).contains(tmp1)) {
						listeToSend.get(0).add(tmp1);  // nom agent
						listeToSend.get(1).add(tmp2);  // sa capacité (moyen d'ajouter d'autres)
					}
				}
				
				// envoyer au silo ce qui lui manque
				ACLMessage NewList = new ACLMessage(ACLMessage.INFORM);
				NewList.setProtocol("list_agentsWithCompleteMap");
				NewList.setSender(this.myAgent.getAID());
				NewList.addReceiver(new AID("Tanker",AID.ISLOCALNAME));
				try {					
					NewList.setContentObject((Serializable) listeToSend);  // ** erreur possibe
				} catch (IOException e) {
					e.printStackTrace();
				}
				((AbstractDedaleAgent)this.myAgent).sendMessage(NewList);
				
				// complèter la sienne si besoin              *** modification possible
				for(int j=0; j<listeReceived.get(0).size();j=j+1) {
					String tmp1 =listeReceived.get(0).get(j);
					String tmp2 =listeReceived.get(1).get(j);
					// si l'agent n'existe pas dans sa liste, il l'ajoute, ainsi que ses capacités 
					if(!this.list_agentsWithCompleteMap.get(0).contains(tmp1)) {
						this.list_agentsWithCompleteMap.get(0).add(tmp1);  // nom agent
						this.list_agentsWithCompleteMap.get(1).add(tmp2);  // sa capacité (moyen d'ajouter d'autres)
					}
				}
				
				
				// passe au mode partage carte avec les autres agents
				//this.myAgent.addBehaviour(new ShareMapAndListBehaviour(this.myAgent, this.list_agentNames, this.myMap, this.list_agentsWithCompleteMap)); //*** à decommenter

			}
			// cas 2 : strategie  
			// on passe en mode collection avec partage de la strategie avec ceux qui l'ont pas
			if(reponse2 != null) {  
				// this.myAgent.addBehaviour();
			}
			
		}
		
		
	}
	
}

