package eu.su.mas.dedaleEtu.communication;

import java.util.List;

import actions.SelectPathAction;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.FSMExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.FSMBehaviour;
import jade.lang.acl.ACLMessage;

public class FSMCommunication extends FSMBehaviour {
	private FSMExploBehaviour caller;
	private MapRepresentation myMap;
	private List<String> list_agentNames;
	private List<ACLMessage> pingMessages;
	private List<ACLMessage> mapMessages;
	
	public FSMCommunication(final AbstractDedaleAgent myagent, FSMExploBehaviour c) {
		super(myagent);
		this.caller = c;
		this.myMap = caller.getMap();
		this.list_agentNames = caller.getListAgent();
		if (this.myMap == null) {
			System.out.print("Grosse cata");
		}
		
		super.registerFirstState(new ReadMessagesCom(this), "read_messages");
		super.registerState(new AnswerPing(this), "answer_ping");
		super.registerState(new AnswerMap(this), "answer_map");
		super.registerLastState(new EndingCommunication(this), "end");
		
		super.registerDefaultTransition("read_messages", "answer_ping");
		super.registerDefaultTransition("answer_ping", "answer_map");
		super.registerDefaultTransition("answer_map", "end", new String[]{"read_messages", "answer_ping", "answer_map"});
	}
	
	public void setPingMessages(List<ACLMessage> p) {
		this.pingMessages = p;
	}
	
	public List<ACLMessage> getPingMessages (){
		return this.pingMessages;
	}
	
	public void setMapMessages(List<ACLMessage> p) {
		this.mapMessages = p;
	}
	
	public List<ACLMessage> getMapMessages (){
		return this.mapMessages;
	}
	
	 public MapRepresentation getMap() {
		 return this.caller.getMap();
	 }
	 
	 public FSMExploBehaviour getCaller() {
		 return this.caller;
	 }
}
