package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.WakerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
//import jade.util.leap.ArrayList;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
//public class PingEmissionBehaviour extends WakerBehaviour{
public class ReceiveMapAndListBehaviour extends SimpleBehaviour{
	
	private MapRepresentation myMap;
	
	private List<String> list_agentNames;
	
	private List<List<String>>  MyCapacities;

	private boolean MapReceived = false;
	
	private int timeForMap;

	private int timeForList;

	private static final long serialVersionUID = -2058134632578521548L;
	
	private boolean finished = false;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public ReceiveMapAndListBehaviour (Agent agent, List<String> list_agentNames, MapRepresentation myMap) {
		super(agent);
		this.list_agentNames=list_agentNames;
		this.myMap = myMap;
		this.timeForMap = 0;
		this.timeForList = 0;
		this.MyCapacities = new ArrayList<>();
	}

	@Override
	public void action(){
		
		
		if (!this.MapReceived) {
			
			MessageTemplate msgTemplate=MessageTemplate.and(
					MessageTemplate.MatchProtocol("SHARE-TOPO"),
					MessageTemplate.MatchPerformative(ACLMessage.INFORM));
			ACLMessage Map=this.myAgent.receive(msgTemplate);
			
			// l'agent attend la map 40ms maximum, en cas d'absence, il reprend son exploration 
			try {
				this.myAgent.doWait(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.timeForMap += 1;
			System.out.println("time : " + this.timeForMap);
			
			if(this.timeForMap > 40 ) {
				System.out.println("Agent "+this.myAgent.getLocalName()+ ": mapComplete non reçue **** time : "+this.timeForMap);
				this.finished = true; 
				this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); 
			}
	
			else if (Map!=null ) {
	
				System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + Map.getSender().getLocalName()+ " :  MapComplete ***** time : "+this.timeForMap);
				
				SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
				try {
					sgreceived = (SerializableSimpleGraph<String, MapAttribute>)Map.getContentObject();
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.myMap.mergeMap(sgreceived);		
				this.MapReceived = true;
				
				// envoie ses capacités 
				ACLMessage capacitiesMsg = new ACLMessage(ACLMessage.INFORM);
				capacitiesMsg.setProtocol("capacités");
				capacitiesMsg.setSender(this.myAgent.getAID());
				capacitiesMsg.addReceiver(Map.getSender());
				
				// preparer le message
				 
				List<String> l1 = new ArrayList<>();
				List<String> l2 = new ArrayList<>();
				l1.add(this.myAgent.getLocalName());
				l2.add("capacités"); // les capacités ******
				this.MyCapacities.add(l1);
				this.MyCapacities.add(l2);
				
				try {					
					capacitiesMsg.setContentObject((Serializable) MyCapacities);  // ** erreur possibe
				} catch (IOException e) {
					e.printStackTrace();
				}
				((AbstractDedaleAgent)this.myAgent).sendMessage(capacitiesMsg);
				System.out.println("Envoi : Agent "+this.myAgent.getLocalName()+ " ===>" + Map.getSender().getLocalName()+ " :  les capacités");
			}
		}
		else {  // il a reçu la Map
			// attend à recevoir la liste
			MessageTemplate msgTemplate2=MessageTemplate.and(
					MessageTemplate.MatchProtocol("list_agentsWithCompleteMap"),
					MessageTemplate.MatchPerformative(ACLMessage.INFORM));
			ACLMessage List=this.myAgent.receive(msgTemplate2);
			
			// l'agent attend la liste 40ms maximum, en cas d'absence, il va voir le Silo 
			try {
				this.myAgent.doWait(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.timeForList += 1;
			System.out.println("time : " + this.timeForList);
			
			if(this.timeForList > 40 ) {
				System.out.println("Agent "+this.myAgent.getLocalName()+ ": List non reçue **** time : "+this.timeForList);
				this.finished = true; 
				// il va rencontrer le Silo
				this.myAgent.addBehaviour(new MeetSiloBehaviour((AbstractDedaleAgent) this.myAgent,this.list_agentNames, this.myMap,MyCapacities)); 

			}
	
			else if (List!=null ) {
	
				System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + List.getSender().getLocalName()+ " :  List ***** time : "+this.timeForList);
				
				List<List<String>> listeReceived=null;
				try {
					listeReceived = (List<List<String>>)List.getContentObject();
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				// rencontrer le Silo
			}
		}
	}
	
	@Override
	public boolean done() {
		return this.finished;
	}
}

