package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.gs.gsLocation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.behaviours.ShareMapBehaviour;


import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;


/**
 * <pre>
 * This behaviour allows an agent to explore the environment and learn the associated topological map.
 * The algorithm is a pseudo - DFS computationally consuming because its not optimised at all.
 * 
 * When all the nodes around him are visited, the agent randomly select an open node and go there to restart its dfs. 
 * This (non optimal) behaviour is done until all nodes are explored. 
 * 
 * Warning, this behaviour does not save the content of visited nodes, only the topology.
 * Warning, the sub-behaviour ShareMap periodically share the whole map
 * </pre>
 * @author hc
 *
 */
public class ExploTestNewCoopBehaviour2 extends OneShotBehaviour { 
	private static final long serialVersionUID = 8567689731496787661L;

	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	private List<String> list_agentNames;

/**
 * 
 * @param myagent
 * @param myMap known map of the world the agent is living in
 * @param agentNames name of the agents to share the map with
 */
	public ExploTestNewCoopBehaviour2(final AbstractDedaleAgent myagent, MapRepresentation myMap,List<String> agentNames) {
		super(myagent);
		this.myMap=myMap;
		this.list_agentNames=agentNames;
		this.myAgent= myagent;
		
	}

	@Override
	public void action() {

		if(this.myMap==null) {
			this.myMap= new MapRepresentation();
			//this.myAgent.addBehaviour(new ShareMapBehaviour(this.myAgent,500,this.myMap,list_agentNames));			
		}

		//0) Retrieve the current position
		Location myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		if (myPosition!=null){ 
			
			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//s'il n'a pas de message à traiter, il se déplace, puis envoie un Ping
			final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);	
			ACLMessage msg = this.myAgent.receive(msgTemplate);
			if (msg == null) {
			
				//List of observable from the agent's current position
				List<Couple<Location,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
	
				//1) remove the current node from openlist and add it to closedNodes.
				this.myMap.addNode(myPosition.getLocationId(), MapAttribute.closed);
	
				//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
				String nextNodeId=null;
				Iterator<Couple<Location, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
				while(iter.hasNext()){
					Location accessibleNode=iter.next().getLeft();
					boolean isNewNode=this.myMap.addNewNode(accessibleNode.getLocationId());
					//the node may exist, but not necessarily the edge
					if (myPosition.getLocationId()!=accessibleNode.getLocationId()) {
						this.myMap.addEdge(myPosition.getLocationId(), accessibleNode.getLocationId());
						if (nextNodeId==null && isNewNode) nextNodeId=accessibleNode.getLocationId();
					}
				}
	
				//3) while openNodes is not empty, continues.
				if (!this.myMap.hasOpenNode()){
					//Explo finished
					finished=true;
					System.out.println(this.myAgent.getLocalName()+" - Exploration successufully done, behaviour removed.");
					
					// se déplacer pour rencontrer le Silo
					// liste des agents qu'il connait ayant la carte complète
					List<List<String>> listAgentCarteComplete = new ArrayList<>();  
					List<String> l1 = new ArrayList<>();
					List<String> l2 = new ArrayList<>();
					l1.add(this.myAgent.getLocalName());
					l2.add("capacités"); // les capacités ******
					listAgentCarteComplete.add(l1);
					listAgentCarteComplete.add(l2);
					// listAgentCarteComplete.add(this.myAgent.getLocalName());
					// this.myAgent.addBehaviour(new MeetSiloBehaviour(this.myAgent,list_agentNames, this.myMap,listAgentCarteComplete )); //***********
				}else{
					//4) select next move.
					//4.1 If there exist one open node directly reachable, go for it,
					//	 otherwise choose one from the openNode list, compute the shortestPath and go for it
					if (nextNodeId==null){
						//no directly accessible openNode
						//chose one, compute the path and take the first step.
						nextNodeId=this.myMap.getShortestPathToClosestOpenNode(myPosition.getLocationId()).get(0);//getShortestPath(myPosition,this.openNodes.get(0)).get(0);
						//System.out.println(this.myAgent.getLocalName()+"-- list= "+this.myMap.getOpenNodes()+"| nextNode: "+nextNode);
						
						// print pour aider à résoudre les blocages
						System.out.println(this.myAgent.getLocalName()+" à "+ myPosition.getLocationId()+" Je souhaite aller à => "+nextNodeId);

					}else {
						//System.out.println("nextNode notNUll - "+this.myAgent.getLocalName()+"-- list= "+this.myMap.getOpenNodes()+"\n -- nextNode: "+nextNode);
					}
	
					((AbstractDedaleAgent)this.myAgent).moveTo(new gsLocation(nextNodeId));
					// faire un Ping pour chercher des voisins
					this.myAgent.addBehaviour(new PingBehaviour(this.myAgent,this.list_agentNames,this.myMap));
				}
			}
			// s'il a reçu un message
			else {
				if(msg.getContent().compareTo("Ping")== 0 || 
						msg.getContent().compareTo("Pong")== 0 ||
						msg.getContent().compareTo("Carte Complete ?")== 0) {
					this.myAgent.addBehaviour(new CommunicationTest(this.myAgent,this.list_agentNames,this.myMap,msg));
				} 
				else {
					//System.out.println(msg.getContent());
					msg = null;
					this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); 
				}
				
			}
		}
	}

	//@Override
	//public boolean done() {
	//	return finished;
	//}

}
