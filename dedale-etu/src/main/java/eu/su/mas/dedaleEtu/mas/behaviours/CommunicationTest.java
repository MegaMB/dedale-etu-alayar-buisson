package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.List;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.WakerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

//import jade.util.leap.ArrayList;

/**
 * @author hc
 *
 */
//public class PingEmissionBehaviour extends WakerBehaviour{
public class CommunicationTest extends OneShotBehaviour{
	private MapRepresentation myMap;
	
	private ACLMessage msg;

	private List<String> list_agentNames;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134622078531998L;
	
	private boolean finished = false;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public CommunicationTest (Agent a, List<String> list_agentNames, MapRepresentation myMap, ACLMessage msg) {
		super(a);
		this.list_agentNames=list_agentNames;
		this.myMap = myMap;
		this.msg = msg;
	}

	@Override
	public void action(){
		
		System.out.println(" Agent "+this.myAgent.getLocalName()+" dans class Commu");
		//System.out.println(this.msg.getContent());
		
		//Location myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		
		if(this.msg.getContent().compareTo("Ping")== 0) {
			
			// si l'agent myAgent est plus petit au point de vue du nom => envoie Pong, sinon, fait rien
			if(this.myAgent.getLocalName().compareTo(this.msg.getSender().getLocalName()) < 0) { 
				ACLMessage msg3=new ACLMessage(ACLMessage.INFORM);
				msg3.setSender(this.myAgent.getAID());
				msg3.setProtocol("UselessProtocol");
				
				System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + this.msg.getSender().getLocalName()+ " :  Ping ");
				
				
				msg3.setContent("Pong");
				msg3.addReceiver(this.msg.getSender());
				((AbstractDedaleAgent)this.myAgent).sendMessage(msg3);	
				System.out.println("Envoie : Agent "+this.myAgent.getLocalName()+ " ===>" + this.msg.getSender().getLocalName()+ " :  Pong");
				
				// on ajoute le behaviour de partage de map
				this.myAgent.addBehaviour(new InfAgentShareMapBehaviour(this.myAgent,this.myMap,this.msg.getSender().getLocalName(), this.list_agentNames));						
			}
			else {
				this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); 
			}
		} else if (this.msg.getContent().compareTo("Pong")== 0) {
			// vu que l'agent inf qui envoie Pong, alors l'agent qui la reçoit est l'agent sup
			System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + this.msg.getSender().getLocalName()+ " :  Pong");
			this.myAgent.addBehaviour(new SupAgentShareMapBehaviour(this.myAgent,this.myMap,this.msg.getSender().getLocalName(), this.list_agentNames));
		}
		else if(this.msg.getContent().compareTo("Carte Complete ?")== 0) {
			System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + this.msg.getSender().getLocalName()+ " :  Question ");
			System.out.println("Envoie : Agent "+this.myAgent.getLocalName()+ " ===>" + this.msg.getSender().getLocalName()+ " :  No");
			//this.myAgent.addBehaviour(new ReceiveMapAndListBehaviour(this.myAgent,this.myMap, this.list_agentNames));
		}
		
	}
	

}

