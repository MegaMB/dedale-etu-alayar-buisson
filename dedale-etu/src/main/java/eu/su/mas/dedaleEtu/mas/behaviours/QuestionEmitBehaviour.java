package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.List;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.WakerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
//import jade.util.leap.ArrayList;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */

public class QuestionEmitBehaviour extends OneShotBehaviour{
	private MapRepresentation myMap;
	
	private List<String> list_agentNames;
	
	private List<List<String>> list_agentsWithCompleteMap;

	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134632078521548L;
	

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public QuestionEmitBehaviour (Agent agent, MapRepresentation myMap, List<String> list_agentNames,List<List<String>> list_agentsWithCompleteMap ) {
		super(agent);
		this.list_agentNames=list_agentNames;
		this.myMap = myMap;
		this.list_agentsWithCompleteMap =list_agentsWithCompleteMap;

	}

	@Override
	public void action(){
		
		Location myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		ACLMessage Question =new ACLMessage(ACLMessage.INFORM);
		Question.setSender(this.myAgent.getAID());
		Question.setProtocol("UselessProtocol");
		
		if (myPosition!=null && myPosition.getLocationId()!=""){
			Question.setContent("Carte Complete ?");
			for (String agents : this.list_agentNames) {
				if (agents.compareTo(this.myAgent.getLocalName()) != 0  && agents.compareTo("Tanker") != 0) {
					Question.addReceiver(new AID(agents,AID.ISLOCALNAME));
					System.out.println("Envoie : Agent "+this.myAgent.getLocalName()+ " ===>" +agents+ " :  Carte Complete ?");
				}
			}
			
			//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
			((AbstractDedaleAgent)this.myAgent).sendMessage(Question);

			this.myAgent.addBehaviour(new ShareMapAndListBehaviour((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames,this.list_agentsWithCompleteMap)); 

		}
	}
	
}

