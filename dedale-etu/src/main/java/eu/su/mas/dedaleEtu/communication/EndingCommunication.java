package eu.su.mas.dedaleEtu.communication;

import jade.core.behaviours.OneShotBehaviour;

public class EndingCommunication extends OneShotBehaviour {
	private FSMCommunication caller;
	
	public EndingCommunication (FSMCommunication c) {
		this.caller = c;
	}
	@Override
	public void action() {
		//System.out.print("Ending Behavior");

	}

}
