package eu.su.mas.dedaleEtu.mas.behaviours;

import jade.core.behaviours.OneShotBehaviour;

public class BehaviourSay extends OneShotBehaviour {
	private String declaration;
	
	public BehaviourSay(String s) {
		this.declaration=s;
	}

	@Override
	public void action() {
		System.out.print(declaration);
	}

}
