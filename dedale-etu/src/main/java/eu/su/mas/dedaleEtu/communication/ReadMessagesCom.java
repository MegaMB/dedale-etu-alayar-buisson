package eu.su.mas.dedaleEtu.communication;

import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ReadMessagesCom extends OneShotBehaviour {
	private FSMCommunication caller;
	private List<ACLMessage> pingMessages;
	private List<ACLMessage> mapMessages;
	
	public ReadMessagesCom(FSMCommunication c) {
		this.caller = c;
		pingMessages = null;
		mapMessages = null;
	}

	@Override
	public void action() {
		//Gestion des messages Ping
		MessageTemplate pingMsgTemplate=MessageTemplate.and(
				MessageTemplate.MatchProtocol("PING"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		this.caller.setPingMessages(this.sortMessages(pingMsgTemplate));
		
		MessageTemplate mapMsgTemplate=MessageTemplate.and(
				MessageTemplate.MatchProtocol("SHARE-TOPO"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		this.caller.setMapMessages(this.sortMessages(mapMsgTemplate));
		MessageTemplate collusionMsgTemplate=MessageTemplate.and(
				MessageTemplate.MatchProtocol("COLLUSION"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		//this.caller.setMapMessages(this.sortMessages(mapMsgTemplate));
	}
	
	//Renvoie la liste des messages qui suivent ce template, en éliminant les doublons.
	public List<ACLMessage> sortMessages(MessageTemplate template) {
		List<ACLMessage> res = new ArrayList<ACLMessage>();
		ACLMessage pingMsgReceived = this.myAgent.receive(template);
		boolean shouldIAdd = false;
		
		while (pingMsgReceived!=null) {
			//System.out.print("Ping received \n");
			String content = pingMsgReceived.getContent();
			AID sender = pingMsgReceived.getSender();
			if (res.isEmpty()) {
				shouldIAdd = true;
			} else {
				for (ACLMessage pingMessages : res) {
					String previousContent = pingMessages.getContent();
					AID previousSender = pingMessages.getSender();
					if (previousContent != content || previousSender.getName() != sender.getName()) {
						shouldIAdd = true;
					}
				}
			}
			if (shouldIAdd == true) {
				res.add(pingMsgReceived);
				//System.out.print(pingMsgReceived.getContent());
				//System.out.print("\n");
			}
		pingMsgReceived = this.myAgent.receive(template);
		}
		if (!res.isEmpty()) {
			//System.out.print("Envoie liste ping");
			return res;
		} else {
			return new ArrayList<ACLMessage>();
		}
	}
}
