package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.List;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.WakerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
//import jade.util.leap.ArrayList;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
//public class PingEmissionBehaviour extends WakerBehaviour{
public class PingEmissionBehaviour extends OneShotBehaviour{
	private MapRepresentation myMap;
	
	private List<String> list_agentNames;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134622078521998L;
	
	private boolean finished = false;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public PingEmissionBehaviour (Agent agent, List<String> list_agentNames, MapRepresentation myMap) {
		//super(a, 500);    //*****************************
		super(agent);
		this.list_agentNames=list_agentNames;
		this.myMap = myMap;
	}

	@Override
	public void action(){
		
		if(this.myMap==null) {
			this.myMap= new MapRepresentation();
		}
		Location myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);	
		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		
		if (msg==null) {
		
			//A message is defined by : a performative, a sender, a set of receivers, (a protocol),(a content (and/or contentOBject))
			ACLMessage msg1=new ACLMessage(ACLMessage.INFORM);
			msg1.setSender(this.myAgent.getAID());
			msg1.setProtocol("UselessProtocol");
			
			if (myPosition!=null && myPosition.getLocationId()!=""){
				//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
				msg1.setContent("Ping");
	
				for (String agents : this.list_agentNames) {
					if (agents.compareTo(this.myAgent.getLocalName()) != 0) {
						msg1.addReceiver(new AID(agents,AID.ISLOCALNAME));
					}
				}
				
				//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
				((AbstractDedaleAgent)this.myAgent).sendMessage(msg1); 
				
			}
			
			
		}
		else {						
			if(msg.getContent().compareTo("Ping")== 0) {
				
				// Seulement le petit envoie les Pong, car les Pongs de grand seront inutils de toute façon
				if(msg.getSender().getLocalName().compareTo(this.myAgent.getLocalName()) < 0) {
					ACLMessage msg2=new ACLMessage(ACLMessage.INFORM);
					msg2.setSender(this.myAgent.getAID());
					msg2.setProtocol("UselessProtocol");
					
					System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + msg.getSender().getLocalName()+ " :  Ping");
					msg2.setContent("Pong");
					msg2.addReceiver(msg.getSender());
					((AbstractDedaleAgent)this.myAgent).sendMessage(msg2);

					this.myAgent.addBehaviour(new InfAgentShareMapBehaviour(this.myAgent,this.myMap,msg.getSender().getLocalName(),this.list_agentNames));
				}
			}
			else if(msg.getContent().compareTo("Pong")== 0) {
				
				System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + msg.getSender().getLocalName()+ " :  Pong");

				// on peut enlever ce if car c'est que le grand qui reçoit les pong ***
				if (msg.getSender().getLocalName().compareTo(this.myAgent.getLocalName()) > 0) {
					//share Map							
					this.myAgent.addBehaviour(new SupAgentShareMapBehaviour(this.myAgent,this.myMap,msg.getSender().getLocalName(),this.list_agentNames));
					finished=true;
				}
			}						 	
		}
		finished = true;
	}
	
	//@Override
	//public boolean done() {
	//	return finished;
	//}

}

