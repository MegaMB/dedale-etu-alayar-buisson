package eu.su.mas.dedaleEtu.communication;

import java.io.IOException;
import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class AnswerPing extends OneShotBehaviour {
	private  FSMCommunication caller;
		
	public AnswerPing (FSMCommunication c) {
		this.caller = c;
	}

	@Override
	public void action() {
		MapRepresentation map = this.caller.getMap();
		List<ACLMessage> pingMessages = this.caller.getPingMessages();
		if (!pingMessages.isEmpty()) {
			//System.out.print(pingMessages.size()+" Ping Received\n");
			for (ACLMessage message : pingMessages) {
				int pingSize = Integer.parseInt(message.getContent());
				if (pingSize != this.caller.getMap().getSizeMap()) {
					ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
					msg.setProtocol("SHARE-TOPO");
					msg.setSender(this.myAgent.getAID());
					msg.addReceiver(message.getSender());
					SerializableSimpleGraph<String, MapAttribute> sg=map.getSerializableGraph();
					try {					
						msg.setContentObject(sg);
					} catch (IOException e) {
						e.printStackTrace();
					}
					((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
					//System.out.print("Map sent");
				}
			}
			this.caller.setPingMessages(null);
		}
	}

}
