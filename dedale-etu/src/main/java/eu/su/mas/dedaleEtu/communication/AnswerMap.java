package eu.su.mas.dedaleEtu.communication;

import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public class AnswerMap extends OneShotBehaviour {
	private  FSMCommunication caller;
	
	public AnswerMap (FSMCommunication c) {
		this.caller = c;
	}
	
	@Override
	public void action() {
		MapRepresentation map = this.caller.getMap();
		List<ACLMessage> mapMessages = this.caller.getMapMessages();
		if (mapMessages != null) {
			for (ACLMessage message : mapMessages) {
				//System.out.print("mise à jour de la carte");
				SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
				try {
					sgreceived = (SerializableSimpleGraph<String, MapAttribute>)message.getContentObject();
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				map.mergeMap(sgreceived);
			}
			this.caller.getCaller().setMap(map);
		}
		// ajouté
		else {
			System.out.print("carte non reçu, non traité, soucis");
		}
	}
}
