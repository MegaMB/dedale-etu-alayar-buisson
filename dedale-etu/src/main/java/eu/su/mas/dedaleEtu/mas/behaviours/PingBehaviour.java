package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.List;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.WakerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
//import jade.util.leap.ArrayList;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
//public class PingEmissionBehaviour extends WakerBehaviour{
public class PingBehaviour extends OneShotBehaviour{
	private MapRepresentation myMap;
	
	private List<String> list_agentNames;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134632078521548L;
	
	private boolean finished = false;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public PingBehaviour (Agent agent, List<String> list_agentNames, MapRepresentation myMap) {
		super(agent);
		this.list_agentNames=list_agentNames;
		this.myMap = myMap;
	}

	@Override
	public void action(){
		
		Location myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		ACLMessage msg1=new ACLMessage(ACLMessage.INFORM);
		msg1.setSender(this.myAgent.getAID());
		msg1.setProtocol("UselessProtocol");
		
		if (myPosition!=null && myPosition.getLocationId()!=""){
			//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
			msg1.setContent("Ping");

			for (String agents : this.list_agentNames) {
				if (agents.compareTo(this.myAgent.getLocalName()) != 0) {
					msg1.addReceiver(new AID(agents,AID.ISLOCALNAME));
					System.out.println("Envoie : Agent "+this.myAgent.getLocalName()+ " ===>" +agents+ " :  Ping");
				}
			}
			
			//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg1);

			this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); //

		}
	}
	
}

