package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.gs.gsLocation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.behaviours.ShareMapBehaviour;


import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.core.behaviours.OneShotBehaviour;

import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;


/**
 * <pre>
 *
 * </pre>
 * @author hc
 *
 */
public class ShareMapAndListBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	private List<String> list_agentNames;
	
	private List<List<String>> list_agentsWithCompleteMap;

/**
 * 
 * @param myagent
 * @param myMap known map of the world the agent is living in
 * @param agentNames name of the agents to share the map with
 */
	public ShareMapAndListBehaviour(final AbstractDedaleAgent myAgent, MapRepresentation myMap,List<String> list_agentNames,List<List<String>> list_agentsWithCompleteMap) {
		super(myAgent);
		this.myMap=myMap;
		this.list_agentNames=list_agentNames;
		this.list_agentsWithCompleteMap =list_agentsWithCompleteMap;
	} 

	@Override
	public void action() {

		

		//0) Retrieve the current position
		Location myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		
		
		if (myPosition!=null){
			
			try {
				this.myAgent.doWait(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);	
			ACLMessage msg = this.myAgent.receive(msgTemplate);
			
			if(msg==null) {				
				
				if(this.list_agentsWithCompleteMap.get(0).size()!=list_agentNames.size()) {  // faire attention au changement de la forme de list_agentsWithCompleteMap 
					this.myAgent.addBehaviour(new QuestionEmitBehaviour(this.myAgent,this.myMap,this.list_agentNames,this.list_agentsWithCompleteMap));
				}
				
				//List of observable from the agent's current position
				List<Couple<Location,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition							
				Random r= new Random();
				int moveId=1+r.nextInt(lobs.size()-1);//removing the current position from the list of target, not necessary as to stay is an action but allow quicker random move
	
				//The move action (if any) should be the last action of your behavior
				((AbstractDedaleAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());				
			}
			else {
				if(msg.getContent().compareTo("Yes")== 0 || msg.getContent().compareTo("No")== 0 || msg.getContent().compareTo("Carte Complete ?")== 0){
					// behavior à faire                vvvvvvv (il suit le même schema que ping pong communication 
					//this.myAgent.addBehaviour(new CommunicateMapAndListBehaviour(this.myAgent,this.myMap,this.list_agentNames,this.list_agentsWithCompleteMap, msg));
				}
				else if (msg.getContent().compareTo("Strategie")== 0) {
					// à reflechir
				}
				else {
					msg = null;
					this.myAgent.addBehaviour(new ShareMapAndListBehaviour((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames,this.list_agentsWithCompleteMap)); 
				}
			}	
		}									
	}

}
