package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;

import java.util.ArrayList;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
//import jade.util.leap.ArrayList;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class InfAgentShareMapBehaviour extends OneShotBehaviour{
	public MapRepresentation myMap;
	private String Receiver;
	private List<String> list_agentNames;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to share his Map
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public InfAgentShareMapBehaviour (Agent agent,  MapRepresentation myMap, String Receiver, List<String> list_agentNames) {
		super(agent);
		this.Receiver = Receiver;
		this.list_agentNames=list_agentNames;
		this.myMap = myMap;
		 
	}
	
	
	@Override
	public void action() {
				
		//System.out.println(" Agent "+this.myAgent.getLocalName()+" dans class Inf");

		
		
		// *** à faire : ajouter à la template un filtre de sender 
		MessageTemplate msgTemplate=MessageTemplate.and( 
				MessageTemplate.MatchProtocol("SHARE-TOPO"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived=this.myAgent.receive(msgTemplate);
			
			// l'agent attend la map 2secondes maximum, en cas d'absence, il reprend son exploration 
			int time = 0;
			//while(time>0 && msgReceived==null) {
			while(time<2000 && msgReceived==null) {
				try {
					this.myAgent.doWait(1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				msgReceived=this.myAgent.receive(msgTemplate);					
				time = time + 1;	
			}
			
			if (msgReceived!=null) {
				
				System.out.println("Reception : Agent "+this.myAgent.getLocalName()+ " <===" + msgReceived.getSender().getLocalName()+ " :  Map1 ***** time : "+time);
				System.out.println("Envoie : Agent "+this.myAgent.getLocalName()+ " ===>" + msgReceived.getSender().getLocalName()+ " :  Map2");
					
				// partage sa Map
				List<String> list_agentName = new ArrayList<String>(); 
				list_agentName.add(this.Receiver);
				this.myAgent.addBehaviour(new ShareMapBehaviour(this.myAgent,500,this.myMap,list_agentName));
				
				// adapte sa Map
				SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
				try {
					sgreceived = (SerializableSimpleGraph<String, MapAttribute>) msgReceived.getContentObject();
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.myMap.mergeMap(sgreceived);
				
				// reprend l'exploration
				this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); //
			}
			else {
				System.out.println("Agent "+this.myAgent.getLocalName()+ ": map1 non reçue **** time : "+time);
				this.myAgent.addBehaviour(new ExploTestNewCoopBehaviour2((AbstractDedaleAgent) this.myAgent,this.myMap,this.list_agentNames)); //
			}
		
			
	}
	
	//@Override
	//public boolean done() {
	//	return finished;
	//}
}
		
		
		
		
		
		
		
		
		
