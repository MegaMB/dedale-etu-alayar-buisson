package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.Agent;
//import jade.core.behaviours.WakerBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
//import jade.util.leap.ArrayList;
import eu.su.mas.dedale.env.gs.gsLocation;

public class TankerShareMapBehaviour extends SimpleBehaviour{
	private MapRepresentation myMap;
	
	private List<String> list_agentNames;
	
	private List<List<String>> list_agentsWithCompleteMap;
	
	private Location position;
	
	private boolean finished=false;
	
	private boolean done=false;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134632475521548L;
	/**
	 * @param myagent the agent who posses the behaviour
	 *  
	 */
	public TankerShareMapBehaviour (Agent agent, List<String> list_agentNames, MapRepresentation myMap,List<List<String>> list_agentsWithCompleteMap, Location SiloPosition ) {
		super(agent);
		this.list_agentNames=list_agentNames;
		this.list_agentsWithCompleteMap =list_agentsWithCompleteMap;
		this.myMap = myMap;
		this.position = SiloPosition;
		
	}
	
	@Override
	public void action(){
		// 
		if(!done) {
			((AbstractDedaleAgent)this.myAgent).moveTo(this.position);
			done = true; // peut être reverifié si c'est vide, s'il y a un blocage, etc *********
		}
		
		try {
			this.myAgent.doWait(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		final MessageTemplate listTemplate = MessageTemplate.MatchProtocol("list_agentsWithCompleteMap");	
		ACLMessage listMsg = this.myAgent.receive(listTemplate);
		
		// s'il a reçu une liste des agents ayant la carte complete
		if(listMsg != null) { 
			// récuperer la liste
			List<List<String>> listeReceived=null;
			try {
				listeReceived = (List<List<String>>)listMsg.getContentObject();
				//jade.util.leap.List listeReceived = (jade.util.leap.List)reponse1.getContentObject();
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			
			// si la taille de la liste reçue est égale à la taille de sa liste actuelle, il fait rien (càd l'agent n'a rien ajouté à sa carte
			if(listeReceived.get(0).size() != this.list_agentsWithCompleteMap.get(0).size()) {
				for(int j=0; j<listeReceived.get(0).size();j=j+1) {
					String tmp1 =listeReceived.get(0).get(j);
					String tmp2 =listeReceived.get(1).get(j);
					// si l'agent n'existe pas dans sa liste, il l'ajoute, ainsi que ses capacités également
					if(!this.list_agentsWithCompleteMap.get(0).contains(tmp1)) {
						this.list_agentsWithCompleteMap.get(0).add(tmp1);  // nom agent
						this.list_agentsWithCompleteMap.get(1).add(tmp2);  // sa capacité (moyen d'ajouter d'autres)
					}
				}
			}
		}
		else {   		// s'il n'a pas reçu de liste => un autre type de message ou aucun message
			final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);	
			ACLMessage msg = this.myAgent.receive(msgTemplate);
			
			if (msg!=null) {
				// s'il reçoit Hello Tanker (càd un agent a carte complete vient le voir, ou yes, càd un agent à carte complete a répondu à sa question
				// il partage avec eux la liste
				if(msg.getContent().compareTo("Hello Tanker") == 0 || msg.getContent().compareTo("Yes") == 0 ) {
					// faire partage liste_agentWithCompleteMap ********
					ACLMessage List = new ACLMessage(ACLMessage.INFORM);
					List.setProtocol("list_agentsWithCompleteMap");
					List.setSender(this.myAgent.getAID());
					List.addReceiver(msg.getSender());
					try {					
						List.setContentObject((Serializable) this.list_agentsWithCompleteMap);  // ** erreur possibe
					} catch (IOException e) {
						e.printStackTrace();
					}
					((AbstractDedaleAgent)this.myAgent).sendMessage(List);
					System.out.println("Tanker partage sa liste avec =====> "+msg.getSender());
					
				}
				
				// No = Map incomplete (reponse à la question) 
				if(msg.getContent().compareTo("No") == 0) {
					
					// partage de carte et de list_agentsWithCompleteMap (à voir comment partager list_agentsWithCompleteMap)  *****
					// faire partage liste_agentWithCompleteMap ********
					ACLMessage List = new ACLMessage(ACLMessage.INFORM);
					List.setProtocol("list_agentsWithCompleteMap");
					List.setSender(this.myAgent.getAID());
					List.addReceiver(msg.getSender());
					try {					
						List.setContentObject((Serializable) this.list_agentsWithCompleteMap);  // ** erreur possibe
					} catch (IOException e) {
						e.printStackTrace();
					}
					((AbstractDedaleAgent)this.myAgent).sendMessage(List);
					System.out.println("Tanker partage sa liste avec =====> "+msg.getSender());
					
					
					// partage carte
					ACLMessage Map = new ACLMessage(ACLMessage.INFORM);
					Map.setProtocol("SHARE-TOPO");
					Map.setSender(this.myAgent.getAID());
					Map.addReceiver(msg.getSender());
					
					SerializableSimpleGraph<String, MapAttribute> sg=this.myMap.getSerializableGraph();
					try {					
						Map.setContentObject(sg);
					} catch (IOException e) {
						e.printStackTrace();
					}
					((AbstractDedaleAgent)this.myAgent).sendMessage(Map);
					
				}
				
				// s'il s'agit d'un agent remplaçant du Tanker, et que le Tanker arrive à sa place
				if(msg.getContent().compareTo("Tanker") == 0) {
					System.out.println("Tanker a arrivé à sa place, le remplaçant se deplace");
					// laisser la place au Tanker, et changer behaviour après avoir répondu à tous les messages  ******
				}
			}
			// s'il n'a pas reçu de message :
				// si liste incomplete => il pose la question à ses voisins
				// sinon, il calcule la strategie 
			else {
				if(this.list_agentsWithCompleteMap.get(0).size()!=this.list_agentNames.size()) {  // faire attention au changement de la forme de list_agentsWithCompleteMap 
					ACLMessage Question =new ACLMessage(ACLMessage.INFORM);
					Question.setSender(this.myAgent.getAID());
					Question.setProtocol("Question");
					Question.setContent("Carte Complete ?");
					for (String agents : this.list_agentNames) {
						if (agents.compareTo(this.myAgent.getLocalName()) != 0) {
							Question.addReceiver(new AID(agents,AID.ISLOCALNAME));
							System.out.println("Envoie : Agent "+this.myAgent.getLocalName()+ " ===>" +agents+ " :  Carte Complete ?");
						}
					}
				}
				else {
					// calculer la strategie et passer en mode collecte
					
				}
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	@Override
	public boolean done() {
		return finished;
	}
	
}